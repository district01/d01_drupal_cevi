<?php

namespace Drupal\d01_drupal_cevi\Soap\LPDC\V2;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CeviLPDCClient.
 *
 * @package Drupal\d01_drupal_cevi
 */
class CeviLPDCClient implements CeviLPDCClientInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The cevi soap client.
   *
   * @var \Drupal\d01_drupal_cevi\Soap\LPDC\V2\CeviLPDCSoapClientInterface|null
   */
  protected $soapClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, CeviLPDCSoapClientInterface $soap_client) {
    $this->configFactory = $config_factory->get('d01_drupal_cevi.settings');
    $this->soapClient = $soap_client->getSoapClient();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('d01_drupal_cevi.lpdc2site.v2.soap_client')
    );
  }

  /**
   * Get the application key.
   *
   * @return string
   *   The application key.
   */
  private function getApplicationKey() {
    return $this->configFactory->get('application_key');
  }

  /**
   * Get the organisation key.
   *
   * @return string
   *   The application key.
   */
  private function getOrganisationKey() {
    return $this->configFactory->get('organisation_key');
  }

  /**
   * {@inheritdoc}
   */
  public function getDocumentById(int $id) {
    // Only get products when we have a valid soap client.
    if (!$this->soapClient) {
      return NULL;
    }

    $document = NULL;
    try {
      // Parameter namings are inconsistent in WSDL
      // (theOrganisationKey, organisationKey, organistationKey)
      // So we need to set them per method.
      $data = $this->soapClient->GetDocumentById(
        [
          'id' => $id,
          'applicationKey' => $this->getApplicationKey(),
          'organistationKey' => $this->getOrganisationKey(),
        ]
      );

      if ($data->GetDocumentByIdResult) {
        $doc_result = $this->xmlStringToArray($data->GetDocumentByIdResult);
        $result = isset($doc_result['AttachmentDownloadFile']) ? $doc_result['AttachmentDownloadFile'] : [];
        if (!empty($result)) {
          $document = new CeviLPDCDownload($result);
        }
      }
    }
    catch (\Exception $exception) {
      // When try fails we do nothing and just return an empty array.
    }

    return $document;
  }

  /**
   * {@inheritdoc}
   */
  public function getLpdc() {

    // Only get products when we have a valid soap client.
    if (!$this->soapClient) {
      return [];
    }

    $products = [];
    try {
      // Parameter namings are inconsistent in WSDL
      // (theOrganisationKey, organisationKey, organistationKey)
      // So we need to set them per method.
      $data = $this->soapClient->getLpdc(
        [
          'theApplicationKey' => $this->getApplicationKey(),
          'theOrganisationKey' => $this->getOrganisationKey(),
        ]
      );

      if ($data->GetLpdcResult) {
        $results = $this->xmlStringToArray($data->GetLpdcResult);
        $product_results = isset($results['Products']) ? $results['Products'] : [];

        $products = [];
        if (!empty($product_results)) {
          foreach ($product_results as $key => $product_item) {
            foreach ($product_item as $p_key => $product) {
              $products[] = new CeviLPDCProduct($product);
            }
          }
        }
      }
    }
    catch (\Exception $exception) {
      // When try fails we do nothing and just return an empty array.
    }

    return $products;
  }

  /**
   * {@inheritdoc}
   */
  public function getLpdcExtended() {

    // Only get products when we have a valid soap client.
    if (!$this->soapClient) {
      return [];
    }

    $products = [];
    try {
      // Parameter namings are inconsistent in WSDL
      // (theOrganisationKey, organisationKey, organistationKey)
      // So we need to set them per method.
      $data = $this->soapClient->GetLpdcExtended(
        [
          'applicationKey' => $this->getApplicationKey(),
          'organisationKey' => $this->getOrganisationKey(),
          'includeInternal' => TRUE,
        ]
      );

      if ($data->GetLpdcExtendedResult) {
        $results = $this->xmlStringToArray($data->GetLpdcExtendedResult);
        $product_results = isset($results['Products']) ? $results['Products'] : [];

        $products = [];
        if (!empty($product_results)) {
          foreach ($product_results as $key => $product_item) {
            foreach ($product_item as $p_key => $product) {
              $products[] = new CeviLPDCProduct($product);
            }
          }
        }
      }
    }
    catch (\Exception $exception) {
      // When try fails we do nothing and just return an empty array.
    }

    return $products;
  }

  /**
   * Convert xml string to array.
   *
   * @param string $xml_string
   *   An XML string.
   *
   * @return mixed
   *   An array built from XML.
   */
  private function xmlStringToArray(string $xml_string) {
    $xml = simplexml_load_string($xml_string);
    return json_decode(json_encode($xml), TRUE);
  }

}
