<?php

namespace Drupal\d01_drupal_cevi\Soap\LPDC\V2;

/**
 * Class Cevi Attachment.
 *
 * This class represents a cevi attachment.
 *
 * @package Drupal\d01_drupal_cevi
 */
class CeviLPDCAttachment implements CeviLPDCAttachmentInterface {

  /**
   * Document ID.
   *
   * @var int
   */
  protected $id = NULL;

  /**
   * Title.
   *
   * @var string
   */
  protected $title = NULL;

  /**
   * CeviLPDCAttachment constructor.
   *
   * @param array $xml_string_generated_array
   *   A attachment array generated from XML string.
   */
  public function __construct(array $xml_string_generated_array) {
    if (!empty($xml_string_generated_array)) {
      foreach ($xml_string_generated_array as $key => $value) {

        // Don't set empty values since XML generated array will pas them as
        // empty array instead of string and this causes an typecast error.
        if (!$value || empty($value)) {
          continue;
        }

        // When an array is passed without literal keys, this is most
        // likely a mistake or empty string casted to array.
        // We cannot generate a values if we don't know the key
        // so bypass the value to prevent typecasting errors.
        if (is_array($value) && isset($value[0])) {
          continue;
        }

        // Look for a setter for the value and set value when present.
        $setMethod = 'set' . ucfirst($key);
        if (method_exists($this, $setMethod)) {
          $this->$setMethod($value);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return (int) $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function setId(string $id) {
    $this->id = $id;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle(string $title) {
    $this->title = $title;
  }

}
