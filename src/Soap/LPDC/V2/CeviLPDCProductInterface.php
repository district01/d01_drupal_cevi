<?php

namespace Drupal\d01_drupal_cevi\Soap\LPDC\V2;

/**
 * Interface CeviLPDCProductInterface.
 *
 * @package Drupal\d01_drupal_cevi
 */
interface CeviLPDCProductInterface {

  /**
   * Get the Product ID.
   *
   * @return int
   *   The product ID.
   */
  public function getId();

  /**
   * Set the Product ID.
   *
   * @param int $id
   *   The product ID.
   */
  public function setId(int $id);

  /**
   * Get product name.
   *
   * @return string
   *   The name of the product.
   */
  public function getName();

  /**
   * Set product name.
   *
   * @param string $name
   *   The name of the product.
   */
  public function setName(string $name);

  /**
   * Get product title.
   *
   * @return string
   *   The title of the product.
   */
  public function getTitle();

  /**
   * Set product title.
   *
   * @param string $title
   *   The title of the product.
   */
  public function setTitle(string $title);

  /**
   * Get product description.
   *
   * @return string
   *   The title of the product.
   */
  public function getDescription();

  /**
   * Set product description.
   *
   * @param string $description
   *   The description of the product.
   */
  public function setDescription(string $description);

  /**
   * Get product start date.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime|null
   *   A DrupalDateTime object.
   */
  public function getStartDate();

  /**
   * Set product start date.
   *
   * @param string $start_date
   *   A string representing a date.
   */
  public function setStartDate(string $start_date);

  /**
   * Get product end date.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime|null
   *   A DrupalDateTime object.
   */
  public function getEndDate();

  /**
   * Set product end date.
   *
   * @param string $end_date
   *   A string representing a date.
   */
  public function setEndDate(string $end_date);

  /**
   * Get product conditions.
   *
   * @return string
   *   The conditions of the product.
   */
  public function getConditions();

  /**
   * Set product conditions.
   *
   * @param string $conditions
   *   A string containing product conditions.
   */
  public function setConditions(string $conditions);

  /**
   * Get product bring to apply.
   *
   * @return string
   *   The bring to apply of the product.
   */
  public function getBringToApply();

  /**
   * Set product bring to apply.
   *
   * @param string $bring_to_apply
   *   A string containing bring to apply.
   */
  public function setBringToApply(string $bring_to_apply);

  /**
   * Get product legal text.
   *
   * @return string
   *   The legal text of the product.
   */
  public function getLegalText();

  /**
   * Set product legal.
   *
   * @param string $legal_text
   *   A string containing product legal text.
   */
  public function setLegalText(string $legal_text);

  /**
   * Get product amount to apply.
   *
   * @return string
   *   The ammount to apply of the product.
   */
  public function getAmountToApply();

  /**
   * Set product amount to apply.
   *
   * @param string $amount_to_apply
   *   A string containing product amount to apply.
   */
  public function setAmountToApply(string $amount_to_apply);

  /**
   * Get product procedure.
   *
   * @return string
   *   The procedure of the product.
   */
  public function getProcedure();

  /**
   * Set product procedure.
   *
   * @param string $procedure
   *   A string containing product procedure.
   */
  public function setProcedure(string $procedure);

  /**
   * Get product exceptions.
   *
   * @return string
   *   The exceptions of the product.
   */
  public function getExceptions();

  /**
   * Set product exceptions.
   *
   * @param string $exceptions
   *   A string containing product exceptions.
   */
  public function setExceptions(string $exceptions);

  /**
   * Get product additional info.
   *
   * @return string
   *   The addition info of the product.
   */
  public function getAdditionalInfo();

  /**
   * Set product additional info.
   *
   * @param string $additional_info
   *   A string containing product additional info.
   */
  public function setAdditionalInfo(string $additional_info);

  /**
   * Get product uploaded attachments.
   *
   * @return \Drupal\d01_drupal_cevi\Soap\LPDC\V2\CeviLPDCAttachmentInterface[]|null
   *   A array of attachment objects.
   */
  public function getUploadedAttachments();

  /**
   * Set product uploaded attachments.
   *
   * @param array $uploaded_attachments
   *   A string containing product additional info.
   */
  public function setUploadedAttachments(array $uploaded_attachments);

  /**
   * Get product timestamp last updated.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime|null
   *   A DrupalDateTime object.
   */
  public function getTimeStampLastUpdate();

  /**
   * Set product timestamp last updated.
   *
   * @param string $timestamp_last_update
   *   A string representing a date.
   */
  public function setTimeStampLastUpdate(string $timestamp_last_update);

  /**
   * Check if product is in internal.
   *
   * @return bool
   *   Indication if product is internal.
   */
  public function isInternal();

  /**
   * Set product internal status.
   *
   * @param bool $isInternal
   *   A boolean indicating if product is internal.
   */
  public function setIsInternal(bool $isInternal);

}
