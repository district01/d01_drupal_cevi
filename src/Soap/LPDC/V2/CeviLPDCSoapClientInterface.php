<?php

namespace Drupal\d01_drupal_cevi\Soap\LPDC\V2;

/**
 * Interface CeviLPDCSoapClientInterface.
 *
 * @package Drupal\d01_drupal_cevi
 */
interface CeviLPDCSoapClientInterface {

  /**
   * Get the service request.
   *
   * @return \Drupal\d01_drupal_cevi\CeviServiceRequestInterface
   *   A service request.
   */
  public function getServiceRequest();

  /**
   * Get the request wsdl url.
   *
   * @return string
   *   A wsdl url.
   */
  public function getRequestWsdlUrl();

  /**
   * Get soap client.
   *
   * @return \SoapClient
   *   An initialized php soap client.
   */
  public function getSoapClient();

}
