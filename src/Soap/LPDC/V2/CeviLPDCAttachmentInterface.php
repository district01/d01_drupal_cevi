<?php

namespace Drupal\d01_drupal_cevi\Soap\LPDC\V2;

/**
 * Interface CeviLPDCAttachmentInterface.
 *
 * @package Drupal\d01_drupal_cevi
 */
interface CeviLPDCAttachmentInterface {

  /**
   * Get the Product ID.
   *
   * @return int
   *   The product ID.
   */
  public function getId();

  /**
   * Set the Product ID.
   *
   * @param string $id
   *   The product ID.
   */
  public function setId(string $id);

  /**
   * Get product title.
   *
   * @return string
   *   The title of the product.
   */
  public function getTitle();

  /**
   * Set product title.
   *
   * @param string $title
   *   The title of the product.
   */
  public function setTitle(string $title);

}
