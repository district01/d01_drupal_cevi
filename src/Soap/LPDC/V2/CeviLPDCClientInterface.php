<?php

namespace Drupal\d01_drupal_cevi\Soap\LPDC\V2;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Interface CeviLPDCClientInterface.
 *
 * @package Drupal\d01_drupal_cevi
 */
interface CeviLPDCClientInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory.
   * @param \Drupal\d01_drupal_cevi\Soap\LPDC\V2\CeviLPDCSoapClientInterface $soap_client
   *   A Cevi LPDC soap client.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CeviLPDCSoapClientInterface $soap_client);

  /**
   * Get document by id method on wsdl.
   *
   * @param int $id
   *   The product id.
   */
  public function getDocumentById(int $id);

  /**
   * Get LPDC method on wsdl.
   *
   * @return \Drupal\d01_drupal_cevi\Soap\LPDC\V2\CeviLPDCProductInterface[]
   *   An array of Cevi LPDC products.
   */
  public function getLpdc();

  /**
   * Get LPDC Extended method on wsdl.
   *
   * @return \Drupal\d01_drupal_cevi\Soap\LPDC\V2\CeviLPDCProductInterface[]
   *   An array of Cevi LPDC products.
   */
  public function getLpdcExtended();

}
