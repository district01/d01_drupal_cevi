<?php

namespace Drupal\d01_drupal_cevi\Soap\LPDC\V2;

use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Class Cevi Product.
 *
 * This class represents a cevi product.
 *
 * @package Drupal\d01_drupal_cevi
 */
class CeviLPDCProduct implements CeviLPDCProductInterface {

  const DEFAULT_STORAGE_TIMEZONE = 'UTC';
  const DEFAULT_STORAGE_FORMAT = 'Y-m-d\TH:i:s';

  /**
   * Product ID.
   *
   * @var int
   */
  protected $id = NULL;

  /**
   * Name.
   *
   * @var string
   */
  protected $name = NULL;

  /**
   * Title.
   *
   * @var string
   */
  protected $title = NULL;

  /**
   * Description.
   *
   * @var string
   */
  protected $description = NULL;

  /**
   * Start Date.
   *
   * @var \Drupal\Core\Datetime\DrupalDateTime
   */
  protected $startDate = NULL;

  /**
   * End Date.
   *
   * @var \Drupal\Core\Datetime\DrupalDateTime
   */
  protected $endDate = NULL;

  /**
   * Conditions.
   *
   * @var string
   */
  protected $conditions = NULL;

  /**
   * BringToApply.
   *
   * @var string
   */
  protected $bringToApply = NULL;

  /**
   * LegalText.
   *
   * @var string
   */
  protected $legalText = NULL;

  /**
   * AmountToApply.
   *
   * @var string
   */
  protected $amountToApply = NULL;

  /**
   * Procedure.
   *
   * @var string
   */
  protected $procedure = NULL;

  /**
   * Exceptions.
   *
   * @var string
   */
  protected $exceptions = NULL;

  /**
   * AdditionalInfo.
   *
   * @var string
   */
  protected $additionalInfo = NULL;

  /**
   * Uploaded attachments.
   *
   * @var \Drupal\d01_drupal_cevi\Soap\LPDC\V2\CeviLPDCAttachmentInterface[]|null
   */
  protected $uploadedAttachments = NULL;

  /**
   * TimeStampLastUpdate.
   *
   * @var \Drupal\Core\Datetime\DrupalDateTime
   */
  protected $timeStampLastUpdate = NULL;

  /**
   * IsInternal.
   *
   * @var bool
   */
  protected $isInternal = NULL;

  /**
   * CeviProduct constructor.
   *
   * @param array $xml_string_generated_array
   *   A product array generated from XML string.
   */
  public function __construct(array $xml_string_generated_array) {
    if (!empty($xml_string_generated_array)) {
      foreach ($xml_string_generated_array as $key => $value) {

        // Don't set empty values since XML generated array will pas them as
        // empty array instead of string and this causes an typecast error.
        if (!$value || empty($value)) {
          continue;
        }

        // When an array is passed without literal keys, this is most
        // likely a mistake or empty string casted to array.
        // We cannot generate a values if we don't know the key
        // so bypass the value to prevent typecasting errors.
        if (is_array($value) && isset($value[0])) {
          continue;
        }

        // Look for a setter for the value and set value when present.
        $setMethod = 'set' . ucfirst($key);
        if (method_exists($this, $setMethod)) {
          $this->$setMethod($value);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return (int) $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function setId(int $id) {
    $this->id = $id;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function setName(string $name) {
    $this->name = $name;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle(string $title) {
    $this->title = $title;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription(string $description) {
    $this->description = $description;
  }

  /**
   * {@inheritdoc}
   */
  public function getStartDate() {
    return $this->startDate;
  }

  /**
   * {@inheritdoc}
   */
  public function setStartDate(string $start_date) {
    $date = NULL;
    if ($start_date) {
      $date = new DrupalDateTime($start_date, CeviLPDCProduct::DEFAULT_STORAGE_TIMEZONE);
    }
    $this->startDate = $date;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndDate() {
    return $this->endDate;
  }

  /**
   * {@inheritdoc}
   */
  public function setEndDate(string $end_date) {
    $date = NULL;
    if ($end_date) {
      $date = new DrupalDateTime($end_date, CeviLPDCProduct::DEFAULT_STORAGE_TIMEZONE);
    }
    $this->endDate = $date;
  }

  /**
   * {@inheritdoc}
   */
  public function getConditions() {
    return $this->conditions;
  }

  /**
   * {@inheritdoc}
   */
  public function setConditions(string $conditions) {
    $this->conditions = $conditions;
  }

  /**
   * {@inheritdoc}
   */
  public function getBringToApply() {
    return $this->bringToApply;
  }

  /**
   * {@inheritdoc}
   */
  public function setBringToApply(string $bring_to_apply) {
    $this->bringToApply = $bring_to_apply;
  }

  /**
   * {@inheritdoc}
   */
  public function getLegalText() {
    return $this->legalText;
  }

  /**
   * {@inheritdoc}
   */
  public function setLegalText(string $legal_text) {
    $this->legalText = $legal_text;
  }

  /**
   * {@inheritdoc}
   */
  public function getAmountToApply() {
    return $this->amountToApply;
  }

  /**
   * {@inheritdoc}
   */
  public function setAmountToApply(string $amount_to_apply) {
    $this->amountToApply = $amount_to_apply;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcedure() {
    return $this->procedure;
  }

  /**
   * {@inheritdoc}
   */
  public function setProcedure(string $procedure) {
    $this->procedure = $procedure;
  }

  /**
   * {@inheritdoc}
   */
  public function getExceptions() {
    return $this->exceptions;
  }

  /**
   * {@inheritdoc}
   */
  public function setExceptions(string $exceptions) {
    $this->exceptions = $exceptions;
  }

  /**
   * {@inheritdoc}
   */
  public function getAdditionalInfo() {
    return $this->additionalInfo;
  }

  /**
   * {@inheritdoc}
   */
  public function setAdditionalInfo(string $additional_info) {
    $this->additionalInfo = $additional_info;
  }

  /**
   * {@inheritdoc}
   */
  public function getUploadedAttachments() {
    return $this->uploadedAttachments;
  }

  /**
   * {@inheritdoc}
   */
  public function setUploadedAttachments(array $uploaded_attachments) {
    $attachments = [];
    if (!empty($uploaded_attachments)) {
      foreach ($uploaded_attachments as $key => $uploaded_attachment_item) {

        // The uploaded attachments are transmitted in a different way
        // from the service depending on whether it is a single attachment
        // or multiple attachments. To accommodate this, we provide an extra
        // check on the key "Id". When the id is present, we know it's a single
        // item so we wrap it up in an array in order to stay consistent.
        if (isset($uploaded_attachment_item['Id'])) {
          $uploaded_attachment_item = [0 => $uploaded_attachment_item];
        }

        if (is_array($uploaded_attachment_item) && !empty($uploaded_attachment_item)) {
          foreach ($uploaded_attachment_item as $a_key => $uploaded_attachment) {

            // Make sure we have a key "Id", this way the import will not fail
            // if the passed array is wrongly formatted.
            if (isset($uploaded_attachment['Id'])) {
              $attachments[] = new CeviLPDCAttachment($uploaded_attachment);
            }
            else {
              \Drupal::logger('d01_drupal_cevi')
                ->error('The product "@product" contains a wrongly formatted attachment.', [
                  '@product' => $this->getTitle(),
                ]);
            }
          }
        }
      }
    }
    $this->uploadedAttachments = $attachments;
  }

  /**
   * {@inheritdoc}
   */
  public function getTimeStampLastUpdate() {
    return $this->timeStampLastUpdate;
  }

  /**
   * {@inheritdoc}
   */
  public function setTimeStampLastUpdate(string $timestamp_last_update) {
    $date = NULL;
    if ($timestamp_last_update) {
      $date = new DrupalDateTime($timestamp_last_update, CeviLPDCProduct::DEFAULT_STORAGE_TIMEZONE);
    }
    $this->timeStampLastUpdate = $date;
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return $this->isInternal;
  }

  /**
   * {@inheritdoc}
   */
  public function setIsInternal(bool $isInternal) {
    $this->isInternal = $isInternal;
  }

}
