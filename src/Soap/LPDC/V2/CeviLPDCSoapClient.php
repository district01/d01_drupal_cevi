<?php

namespace Drupal\d01_drupal_cevi\Soap\LPDC\V2;

use Drupal\d01_drupal_cevi\CeviServiceRequest;

/**
 * Class CeviLPDCSoapClient.
 *
 * @package Drupal\d01_drupal_cevi
 */
class CeviLPDCSoapClient implements CeviLPDCSoapClientInterface {

  const WSDL = 'http://wscatalog.cevi.be/WSCatalog.svc?wsdl';
  const CLIENT_VERSION = 2;
  const SERVICE_NAME = 'LPDC2Site';

  /**
   * The service request.
   *
   * @var \Drupal\d01_drupal_cevi\CeviServiceRequest
   */
  protected $serviceRequest;

  /**
   * The requested wsdl url.
   *
   * @var string
   */
  protected $requestWsdlUrl;

  /**
   * The soap client.
   *
   * @var \SoapClient
   */
  protected $soapClient;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->serviceRequest = $this->prepareServiceRequest();
    $this->requestWsdlUrl = $this->prepareRequestWsdlUrl();
    $this->soapClient = $this->prepareSoapClient();
  }

  /**
   * Get the service request.
   *
   * @return \Drupal\d01_drupal_cevi\CeviServiceRequest
   *   A service request.
   */
  public function getServiceRequest() {
    return $this->serviceRequest;
  }

  /**
   * Get the request wsdl url.
   *
   * @return string
   *   A url string.
   */
  public function getRequestWsdlUrl() {
    return $this->requestWsdlUrl;
  }

  /**
   * Get soap client.
   *
   * @return \SoapClient
   *   An initialized php soap client.
   */
  public function getSoapClient() {
    return $this->soapClient;
  }

  /**
   * {@inheritdoc}
   */
  private function prepareServiceRequest() {
    return new CeviServiceRequest(
      CeviLPDCSoapClient::CLIENT_VERSION,
      CeviLPDCSoapClient::SERVICE_NAME
    );
  }

  /**
   * {@inheritdoc}
   */
  private function prepareRequestWsdlUrl() {
    $soap_client = new \SoapClient(CeviLPDCSoapClient::WSDL);
    try {
      $url_object = $soap_client->getUrl(
        [
          'serviceRequest' => $this->getServiceRequest(),
        ]
      );
      return $url_object->GetUrlResult . '?wsdl';
    }
    catch (\Exception $exception) {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  private function prepareSoapClient() {

    // When no WSDL url is found we can not build a soap client.
    if (!$this->getRequestWsdlUrl()) {
      return NULL;
    }

    // Try to build Soap client.
    try {
      return new \SoapClient($this->getRequestWsdlUrl());
    }
    catch (\Exception $exception) {
      return NULL;
    }
  }
}
