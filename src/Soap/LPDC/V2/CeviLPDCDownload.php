<?php

namespace Drupal\d01_drupal_cevi\Soap\LPDC\V2;

use Drupal\Core\Url;

/**
 * Class Cevi Attachment download.
 *
 * This class represents a cevi attachment download.
 *
 * @package Drupal\d01_drupal_cevi
 */
class CeviLPDCDownload implements CeviLPDCDownloadInterface {

  const DOWNLOAD_URL = '/cevi/document/';

  /**
   * The mime type guesser.
   *
   * @var \Drupal\Core\File\MimeType\ExtensionMimeTypeGuesser
   */
  protected $mimeTypeGuesser;

  /**
   * Base64 encoded string.
   *
   * @var string
   */
  protected $fileBase64 = NULL;

  /**
   * Filename.
   *
   * @var string
   */
  protected $filename = NULL;

  /**
   * CeviLPDCDownload constructor.
   *
   * @param array $xml_string_generated_array
   *   A download array generated from XML string.
   */
  public function __construct(array $xml_string_generated_array) {
    $this->mimeTypeGuesser = \Drupal::service('file.mime_type.guesser.extension');


    if (!empty($xml_string_generated_array)) {

      foreach ($xml_string_generated_array as $key => $value) {
        // Don't set empty values since XML generated array will pas them as
        // empty array instead of string and this causes an typecast error.
        if (!$value || empty($value)) {
          continue;
        }

        // When an array is passed without literal keys, this is most
        // likely a mistake or empty string casted to array.
        // We cannot generate a values if we don't know the key
        // so bypass the value to prevent typecasting errors.
        if (is_array($value) && isset($value[0])) {
          continue;
        }

        // Look for a setter for the value and set value when present.
        $setMethod = 'set' . ucfirst($key);
        if (method_exists($this, $setMethod)) {
          $this->$setMethod($value);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFilename() {
    return $this->filename;
  }

  /**
   * {@inheritdoc}
   */
  public function setFilename(string $filename) {
    $this->filename = $filename;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileBase64() {
    return $this->fileBase64;
  }

  /**
   * {@inheritdoc}
   */
  public function setFileBase64(string $file_base64) {
    $this->fileBase64 = $file_base64;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileDecoded() {
    return base64_decode($this->fileBase64);
  }

  /**
   * {@inheritdoc}
   */
  public function getDownloadUrl($id) {
    return Url::fromRoute('d01_drupal_cevi.document', ['id' => $id]);
  }

  /**
   * {@inheritdoc}
   */
  public function getMimeType() {
    return $this->mimeTypeGuesser->guess($this->getFilename());
  }

  /**
   * {@inheritdoc}
   */
  public function getContentLength() {
    $data = $this->getFileDecoded();
    return strlen($data);
  }

}
