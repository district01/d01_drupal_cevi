<?php

namespace Drupal\d01_drupal_cevi\Soap\LPDC\V2;

/**
 * Interface CeviLPDCAttachmentDownloadInterface.
 *
 * @package Drupal\d01_drupal_cevi
 */
interface CeviLPDCDownloadInterface {

  /**
   * Get filename.
   *
   * @return string
   *   The filename of the download.
   */
  public function getFilename();

  /**
   * Set filename.
   *
   * @param string $filename
   *   The filename of the download.
   */
  public function setFilename(string $filename);

  /**
   * Get base64 encoded file.
   *
   * @return string
   *   The base64 encoded string.
   */
  public function getFileBase64();

  /**
   * Get base64 decoded file.
   *
   * @return string
   *   The base64 decoded string.
   */
  public function getFileDecoded();

  /**
   * Set base64 encoded file.
   *
   * @param string $file_base64
   *   The base64 encoded file.
   */
  public function setFileBase64(string $file_base64);

  /**
   * Get an url to download the file.
   *
   * @return string
   *   The url to download the file.
   */
  public function getDownloadUrl($id);

  /**
   * Get the file mime type.
   *
   * @return string
   *   The file mime type.
   */
  public function getMimeType();

  /**
   * Get the file content length.
   *
   * @return string
   *   The file content length.
   */
  public function getContentLength();

}
