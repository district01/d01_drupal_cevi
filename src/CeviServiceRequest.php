<?php

namespace Drupal\d01_drupal_cevi;

/**
 * Class for CeviServiceRequest.
 *
 * This class was built from the WSDL provided by Cevi.
 * {@link http://wscatalog.cevi.be/WSCatalog.svc?xsd=xsd2}
 */
class CeviServiceRequest implements CeviServiceRequestInterface {

  /**
   * The client version.
   *
   * @var int
   */
  public $ClientVersion;

  /**
   * The service name.
   *
   * @var string
   */
  public $ServiceName;

  /**
   * Constructor.
   *
   * @param int $client_version
   *   The client version.
   * @param string $service_name
   *   The service name.
   */
  public function __construct($client_version = NULL, $service_name = NULL) {
    $this->ClientVersion = $client_version;
    $this->ServiceName = $service_name;
  }

  /**
   * Get the client version.
   *
   * @return int|null
   *   the client version number.
   */
  public function getClientVersion() {
    return $this->ClientVersion;
  }

  /**
   * Set the client version.
   *
   * @param int $client_version
   *   The client version number.
   */
  public function setClientVersion($client_version) {
    $this->ClientVersion = $client_version;
  }

  /**
   * Get the service name.
   *
   * @return string|null
   *   the service name.
   */
  public function getServiceName() {
    return $this->ServiceName;
  }

  /**
   * Set the service name.
   *
   * @param string $service_name
   *   The service name.
   */
  public function setServiceName($service_name) {
    $this->ServiceName = $service_name;
  }

}
