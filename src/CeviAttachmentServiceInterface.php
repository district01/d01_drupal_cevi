<?php

namespace Drupal\d01_drupal_cevi;

/**
 * Interface CeviAttachmentServiceInterface.
 *
 * @package Drupal\d01_drupal_cevi
 */
interface CeviAttachmentServiceInterface {

  /**
   * Get an url to download the file.
   *
   * @return string
   *   The url to download the file.
   */
  public function getDownloadUrl($id);

}
