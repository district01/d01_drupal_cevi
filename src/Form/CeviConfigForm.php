<?php

namespace Drupal\d01_drupal_cevi\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration for the cevi service.
 */
class CeviConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'd01_drupal_cevi_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'd01_drupal_cevi.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('d01_drupal_cevi.settings');

    $form['application_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Application key'),
      '#default_value' => $config->get('application_key'),
    ];

    $form['organisation_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Organisation key'),
      '#default_value' => $config->get('organisation_key'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('d01_drupal_cevi.settings')
      ->set('application_key', $form_state->getValue('application_key'))
      ->set('organisation_key', $form_state->getValue('organisation_key'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
