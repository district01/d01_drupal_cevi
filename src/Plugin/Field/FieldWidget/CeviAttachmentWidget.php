<?php

namespace Drupal\d01_drupal_cevi\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Widget for the "Cevi attachment" field.
 *
 * @FieldWidget(
 *   id = "d01_drupal_cevi_attachment_widget",
 *   label = @Translation("Cevi attachment widget"),
 *   field_types = {
 *     "d01_drupal_cevi_attachment"
 *   }
 * )
 */
class CeviAttachmentWidget extends WidgetBase implements WidgetInterface {

  const EDITABLE = FALSE;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'editable' => CeviAttachmentWidget::EDITABLE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = t('Make field editable: @editable', [
      '@editable' => $this->getSetting('editable') ? t('Yes') : t('No'),
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['editable'] = [
      '#type' => 'checkbox',
      '#title' => t('Allow the user to edit this field'),
      '#default_value' => $this->getSetting('editable'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [
      '#type' => 'fieldset',
    ];

    $id = isset($items[$delta]->id) ? $items[$delta]->id : NULL;
    $element['id'] = [
      '#title' => t('ID'),
      '#type' => 'number',
      '#default_value' => $id,
      '#disabled' => !$this->getSetting('editable'),
    ];

    $content_length = isset($items[$delta]->content_length) ? $items[$delta]->content_length : NULL;
    $element['content_length'] = [
      '#title' => t('Content length'),
      '#type' => 'number',
      '#default_value' => $content_length,
      '#field_suffix' => t('bytes'),
      '#disabled' => !$this->getSetting('editable'),
    ];

    $file_name = isset($items[$delta]->file_name) ? $items[$delta]->file_name : NULL;
    $element['file_name'] = [
      '#title' => t('Filename'),
      '#type' => 'textfield',
      '#default_value' => $file_name,
      '#disabled' => !$this->getSetting('editable'),
    ];

    $mime_type = isset($items[$delta]->mime_type) ? $items[$delta]->mime_type : NULL;
    $element['mime_type'] = [
      '#title' => t('Mime type'),
      '#type' => 'textfield',
      '#default_value' => $mime_type,
      '#disabled' => !$this->getSetting('editable'),
    ];

    return $element;
  }

}
