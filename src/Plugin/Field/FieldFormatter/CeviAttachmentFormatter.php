<?php

namespace Drupal\d01_drupal_cevi\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Formatter for the "Cevi attachment" field.
 *
 * @FieldFormatter(
 *   id = "d01_drupal_cevi_attachment_formatter",
 *   label = @Translation("Cevi attachment formatter"),
 *   field_types = {
 *     "d01_drupal_cevi_attachment"
 *   }
 * )
 */
class CeviAttachmentFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $attachments, $langcode) {
    $element = [];

    foreach ($attachments->getIterator() as $delta => $attachment) {
      $element[$delta]['id'] = [
        '#markup' => $attachment->id,
      ];

      $element[$delta]['file_name'] = [
        '#markup' => $attachment->file_name,
      ];

      $element[$delta]['mime_type'] = [
        '#markup' => $attachment->mime_type,
      ];

      $element[$delta]['content_length'] = [
        '#markup' => $attachment->content_length,
      ];
    }

    return $element;
  }

}
