<?php

namespace Drupal\d01_drupal_cevi\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type for saving cevi attachment metadata.
 *
 * @FieldType(
 *   id = "d01_drupal_cevi_attachment",
 *   label = @Translation("Cevi attachment"),
 *   category = @Translation("Cevi"),
 *   default_formatter = "d01_drupal_cevi_attachment_formatter",
 *   default_widget = "d01_drupal_cevi_attachment_widget",
 * )
 */
class CeviAttachment extends FieldItemBase implements FieldItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'id' => [
          'type' => 'int',
          'length' => 20,
          'not null' => TRUE,
        ],
        'file_name' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ],
        'mime_type' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ],
        'content_length' => [
          'type' => 'int',
          'length' => 20,
          'not null' => FALSE,
        ]
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['id'] = DataDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setRequired(TRUE);

    $properties['file_name'] = DataDefinition::create('string')
      ->setLabel(t('Filename'))
      ->addConstraint('Length', ['max' => 255]);

    $properties['mime_type'] = DataDefinition::create('string')
      ->setLabel(t('Mimetype'))
      ->addConstraint('Length', ['max' => 255]);

    $properties['content_length'] = DataDefinition::create('integer')
      ->setLabel(t('Content length'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    if (empty($this->get('id')->getValue())) {
      return TRUE;
    }

    return FALSE;
  }

}
