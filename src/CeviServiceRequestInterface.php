<?php

namespace Drupal\d01_drupal_cevi;

/**
 * Interface CeviServiceRequestInterface.
 *
 * @package Drupal\d01_drupal_cevi
 */
interface CeviServiceRequestInterface {

  /**
   * Constructor.
   *
   * @param int $client_version
   *   The client version.
   * @param string $service_name
   *   The service name.
   */
  public function __construct($client_version = NULL, $service_name = NULL);

  /**
   * Get the client version.
   *
   * @return int|null
   *   the client version number.
   */
  public function getClientVersion();

  /**
   * Set the client version.
   *
   * @param int $client_version
   *   The client version number.
   */
  public function setClientVersion($client_version);

  /**
   * Get the service name.
   *
   * @return string|null
   *   the service name.
   */
  public function getServiceName();

  /**
   * Set the service name.
   *
   * @param string $service_name
   *   The service name.
   */
  public function setServiceName($service_name);

}
