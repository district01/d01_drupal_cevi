<?php

namespace Drupal\d01_drupal_cevi;

use Drupal\Core\Url;

/**
 * Class for CeviAttachmentService.
 */
class CeviAttachmentService implements CeviAttachmentServiceInterface {

  /**
   * {@inheritdoc}
   */
  public function getDownloadUrl($id) {
    return Url::fromRoute('d01_drupal_cevi.document', ['id' => $id]);
  }

}
