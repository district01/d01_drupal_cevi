<?php

namespace Drupal\d01_drupal_cevi\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\d01_drupal_cevi\Soap\LPDC\V2\CeviLPDCClientInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * CeviDocumentController.
 */
class CeviDocumentController extends ControllerBase {

    /**
     * The cevi client.
     *
     * @var \Drupal\d01_drupal_cevi\Soap\LPDC\V2\CeviLPDCClientInterface
     */
    protected $ceviClient;

    /**
     * The file system.
     *
     * @var \Drupal\Core\File\FileSystemInterface
     */
    protected $fileSystem;

    /**
     * The messenger.
     *
     * @var \Drupal\Core\Messenger\MessengerInterface
     */
    protected $messenger;


    /**
     * Constructor.
     *
     * @param \Drupal\d01_drupal_cevi\Soap\LPDC\V2\CeviLPDCClientInterface $cevi_client
     *   The cevi client.
     *
     * @param \Drupal\Core\File\FileSystemInterface $file_system
     *   The file system service.
     *
     * @param \Drupal\Core\Messenger\MessengerInterface $messenger
     *   The messenger.
     */
    public function __construct(CeviLPDCClientInterface $cevi_client, FileSystemInterface $file_system, MessengerInterface $messenger) {
        $this->ceviClient = $cevi_client;
        $this->fileSystem = $file_system;
        $this->messenger = $messenger;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container) {
        return new static(
            $container->get('d01_drupal_cevi.lpdc2site.v2.client'),
            $container->get('file_system'),
            $container->get('messenger')
        );
    }

    /**
     * Title for download page.
     *
     * @param string $id
     *   The document ID.
     *
     * @return string
     *   The filename.
     */
    public function documentTitle($id) {
        $document = $this->ceviClient->getDocumentById($id);
        return $document->getFilename();
    }

    /**
     * Download action for cevi documents.
     *
     * @param string $id
     *   The document ID.
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     *   A file download.
     */
    public function documentDownload($id) {

        // Get the document.
        $document = $this->ceviClient->getDocumentById($id);

        // When no document provided return page not found.
        if (!$document) {
            throw new NotFoundHttpException();
        }

        // Create a tmp uri.
        $tmp_uri = $this->fileSystem->saveData($document->getFileDecoded(), 'public://', FileSystemInterface::EXISTS_RENAME);

        // When no tmp uri could
        // be created return page not found.
        if (!$tmp_uri) {
            $this->messenger->all();
            throw new NotFoundHttpException();
        }

        // Set the correct headers.
        $headers = [
            'Content-Type' => $document->getMimeType(),
            'Content-Length' => $document->getContentLength(),
            'Cache-Control' => 'private',
        ];

        // Download File.
        return new BinaryFileResponse($tmp_uri, 200, $headers);
    }

}
